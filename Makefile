## -*- Mode: Makefile; -*-                                             
##
## Makefile for some sample C clients of librosie
##
## © Copyright IBM Corporation 2016, 2017, 2018.
## LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
## AUTHOR: Jamie A. Jennings

## Use "DEBUG=1" on the command line to enable logging to stderr

ifdef DEBUG
DEBUGFLAG=-DDEBUG
endif

default: dynamic static mt

REPORTED_PLATFORM=$(shell (uname -o || uname -s) 2> /dev/null)
ifeq ($(REPORTED_PLATFORM), Darwin)
PLATFORM=macosx
else ifeq ($(REPORTED_PLATFORM), GNU/Linux)
PLATFORM=linux
else
PLATFORM=none
endif

ifeq ($(PLATFORM), macosx)
CC= cc
else
CC= gcc
endif

ifeq ($(PLATFORM),macosx)
CFLAGS += -std=c99
SYSCFLAGS=-fPIC -I/usr/local/include
SYSLIBS=-lreadline -lrosie
SYSLDFLAGS=-undefined error -L/usr/local/lib
else 
SYSCFLAGS=-std=c99 -D_GNU_SOURCE=1 -fPIC
SYSLDFLAGS=-Wl,--no-undefined
SYSLIBS=-lpthread -lreadline -ldl -lm -lrosie
endif

CFLAGS += -pthread -O2 -Wall -Wextra $(DEBUGFLAG) $(SYSCFLAGS) $(MYCFLAGS) 
LDFLAGS= $(SYSLDFLAGS) $(MYLDFLAGS)
LIBS= $(SYSLIBS) $(MYLIBS)

AR= ar rcu
RANLIB= ranlib
RM= rm -f

MYCFLAGS=
MYLDFLAGS= 
MYLIBS= 
MYOBJS=

ROSIE_LIB_NAME=librosie
ROSIE_A=$(ROSIE_LIB_NAME).a

dynamic.o: dynamic.c dynamic.h
	$(CC) -o $@ -c dynamic.c $(CFLAGS)

dynamic: dynamic.o 
	$(CC) -o $@ dynamic.o $(SYSLIBS) $(SYSLDFLAGS)

static.o: static.c
	$(CC) -o $@ -c static.c $(CFLAGS)

static: static.o
	$(CC) -o $@ static.o $(SYSLIBS) $(SYSLDFLAGS)

mt.o: mt.c
	$(CC) -o $@ -c mt.c $(CFLAGS)

mt: mt.o
	$(CC) -o $@ mt.o $(SYSLIBS) $(SYSLDFLAGS)

clean:
	$(RM) dynamic.o dynamic
	$(RM) static.o static
	$(RM) mt.o mt

depend:
	@$(CC) $(CFLAGS) -MM *.c

test: static dynamic mt
	@echo Running C client tests on librosie
	@echo
	@echo Statically linked test program:
	./static
	@echo
	@echo Dynamically linked test program:
	./dynamic
	@echo
	@echo Multi-threaded, statically linked test program:
	./mt 4 25 testinput.txt

echo:
	@echo "HOME= $(HOME)"
	@echo "PLAT= $(PLAT)"
	@echo "CC= $(CC)"
	@echo "CFLAGS= $(CFLAGS)"
	@echo "LDFLAGS= $(LDFLAGS)"
	@echo "LIBS= $(LIBS)"
	@echo "RM= $(RM)"

static.o: static.c
dynamic.o: dynamic.c dynamic.h 
mt.o: mt.c
