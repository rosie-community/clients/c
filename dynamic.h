/*  -*- Mode: C/l; -*-                                                       */
/*                                                                           */
/*  dynamic.h                                                                */
/*                                                                           */
/*  © Copyright IBM Corporation 2017.                                        */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

str (*fp_rosie_new_string)(byte_ptr msg, size_t len);
str *(*fp_rosie_new_string_ptr)(byte_ptr msg, size_t len);
void (*fp_rosie_free_string)(str s);
void (*fp_rosie_free_string_ptr)(str *s);

void *(*fp_rosie_new)(str *errors);
void (*fp_rosie_finalize)(void *L);
int (*fp_rosie_libpath)(void *L, char *newpath);
int (*fp_rosie_alloc_limit)(void *L, int *newlimit, int *usage);
int (*fp_rosie_config)(void *L, str *retvals);
int (*fp_rosie_compile)(void *L, str *expression, int *pat, str *errors);
int (*fp_rosie_free_rplx)(void *L, int pat);
int (*fp_rosie_match)(void *L, int pat, int start, char *encoder, str *input, match *match);
int (*fp_rosie_matchfile)(void *L, int pat, char *encoder, int wholefileflag,
		    char *infilename, char *outfilename, char *errfilename,
		    int *cin, int *cout, int *cerr,
		    str *err);
int (*fp_rosie_trace)(void *L, int pat, int start, char *trace_style, str *input, int *matched, str *trace);
int (*fp_rosie_load)(void *L, int *ok, str *src, str *pkgname, str *errors);
int (*fp_rosie_import)(void *L, int *ok, str *pkgname, str *as, str *actual_pkgname, str *errors);

